FROM nginx:alpine
LABEL maintainer="Daniel Cassiero <daniel.cassiero@gmail.com>"

EXPOSE 80

ADD site /usr/share/nginx/html/
